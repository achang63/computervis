# Include necessary packages
from __future__ import print_function
import cv2
import numpy as np
import matplotlib.pyplot as plt

#Question 1
def NCC(img, tmp):
    tmpHeight = tmp.shape[0]
    tmpWidth = tmp.shape[1]
    normTmp =  (tmp - np.mean(tmp))/np.std(tmp)
    normImg = (img - np.mean(img))/np.std(img)
    ncc = np.zeros((img.shape[0]-tmpHeight, img.shape[1]- tmpWidth)).astype(float)
    for ind1 in range(img.shape[0]-tmpHeight):
        for ind2 in range(img.shape[1]-tmpWidth):
            ncc[ind1, ind2] = np.multiply(normImg[ind1:ind1+tmpHeight, ind2:ind2+tmpWidth], normTmp).mean()
    
    return ncc

#Question 2
def Threshold(res):
    thresh = 1.22
    loci = np.argwhere(res>thresh)
    coords = list()
    for item in loci:
        coords.append(tuple(item))
    return coords
    
#Question 3    
def Bounding_box(img_rgb, template, coords):
    img_detected = img_rgb.copy()
    img_detected = cv2.cvtColor(img_detected, cv2.COLOR_BGR2RGB)
    tmpHeight = template.shape[0]
    tmpWidth = template.shape[1]
    for indices in coords:
        cv2.rectangle(img_detected, (indices[1], indices[0]), (indices[1]+ tmpWidth, indices[0] + tmpHeight), (255,0,0), 1)
    
    return img_detected