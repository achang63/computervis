import time
import numpy as np
import urllib
import cv2

#Question 1
def conv(image, kernel):
    avekernel = kernel
    pad = int((avekernel.shape[0]-1)/2)
    kerSize = avekernel.shape[0]
    paddedIm = np.pad(image, pad, 'constant')
    conved_image = image.copy().astype(int)
    for ind1 in range(image.shape[0]):
        for ind2 in range(image.shape[1]):
            val = np.multiply(paddedIm[ind1:ind1+kerSize, ind2:ind2+kerSize], avekernel).sum()
            conved_image[ind1, ind2] = val
    conved_image[conved_image > 255] = 255
    return conved_image.astype(np.uint8)

#Question 2
def downsample(image):
    downsampled_image = np.zeros((int(image.shape[0]/2), int(image.shape[1]/2)))
    for ind1 in range(int(image.shape[0]/2)):
        for ind2 in range(int(image.shape[1]/2)):
            downsampled_image[ind1, ind2] = image[ind1*2, ind2*2]
            
    return downsampled_image 
    
#Question 3    
def gaussianPyramid(image, W, k):
    G = list()
    curImage = image.copy()
    G.append(curImage)
    for iter in range(k-1):
        curImage = curImage.copy()
        curImage = downsample(curImage)
        curImage = conv(curImage,W)
        G.append(curImage)
    return G


#Question 4
def upsample(image):
    upsampled_image = np.zeros((int(image.shape[0]*2), int(image.shape[1]*2)))
    for ind1 in range(int(image.shape[0])):
        for ind2 in range(int(image.shape[1])):
            upsampled_image[ind1*2, ind2*2] = image[ind1, ind2]
    return upsampled_image


#Question 5
def laplacianPyramid(G,W):
    L = list()
    for ind in range(len(G)-1):
        L.append(G[ind]-conv(upsample(G[ind+1]), 4*W) + 128)
    return L