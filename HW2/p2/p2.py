from __future__ import print_function
import cv2
import numpy as np
import math
from scipy.ndimage import filters
from scipy import signal
from matplotlib import pyplot as plt
from non_max_suppression import non_max_suppression

#Question 1
def gaussian_derivative1d(window_size=5, sigma=1):
    sigma = float(sigma)
    x_vals = range(-int(window_size/2), int(window_size/2)+1)
    x_vals = np.array(x_vals)
    f= (np.exp(-np.square(x_vals)/(2*np.square(sigma))))/np.sqrt(2*np.pi*np.square(sigma))
    f = np.multiply(-x_vals/np.square(sigma),f)
    
    return f

#Question 2
def gaussian_filter2d(window_shape=(3,3), sigma=0.5):
    sigma = float(sigma)
    x_vals1 = range(-int(window_shape[0]/2), int(window_shape[0]/2)+1)
    x_vals1 = np.array(x_vals1).astype(float)
    f1 = (np.exp(-np.square(x_vals1)/(2*np.square(sigma))))/np.sqrt(2*np.pi*np.square(sigma))
    x_vals2 = range(-int(window_shape[1]/2), int(window_shape[1]/2)+1)
    x_vals2 = np.array(x_vals2).astype(float)
    f2 = (np.exp(-np.square(x_vals2)/(2*np.square(sigma))))/np.sqrt(2*np.pi*np.square(sigma))
    f = np.matmul(f1.reshape((-1,1)), f2.reshape((1,-1)))  
    f = f/f.sum()
    return f

#Question 3
def harris_corner(image, k, thresh):
    #Finding the derivatives of the image
    x_derivFilt = gaussian_derivative1d(3, 1).reshape((1,-1))
    y_derivFilt = x_derivFilt.transpose()
    derivImX = signal.convolve2d(image, x_derivFilt, 'same', 'symm').astype(float)
    derivImY = signal.convolve2d(image, y_derivFilt, 'same', 'symm').astype(float)
    imX2 = np.multiply(derivImX, derivImX)
    imXY = np.multiply(derivImX, derivImY)
    imY2 = np.multiply(derivImY, derivImY)
    
    #Summing the derivatives of the image
    winSize = 5
    window = gaussian_filter2d((winSize,winSize), 0.5)
    derivImX = np.pad(derivImX, int((winSize-1)/2), 'symmetric')
    derivImX = np.pad(derivImX, int((winSize-1)/2), 'symmetric')
    sumX2 = np.zeros(imX2.shape).astype(float)
    sumXY = np.zeros(imX2.shape).astype(float)
    sumY2 = np.zeros(imX2.shape).astype(float)
    for ind1 in range(image.shape[0]-winSize):
        for ind2 in range(image.shape[1]-winSize):
            sumX2[ind1, ind2] = np.multiply(imX2[ind1:ind1+winSize,ind2:ind2+winSize],window).sum()
            sumXY[ind1, ind2] = np.multiply(imXY[ind1:ind1+winSize,ind2:ind2+winSize],window).sum()
            sumY2[ind1, ind2] = np.multiply(imY2[ind1:ind1+winSize,ind2:ind2+winSize],window).sum()
    
    #Computing the values for R
    rMat = np.zeros(imX2.shape).astype(float)
    for ind1 in range(image.shape[0]):
        for ind2 in range(image.shape[1]):
            hMat = np.array([[sumX2[ind1, ind2],sumXY[ind1, ind2]],[sumXY[ind1, ind2],sumY2[ind1, ind2]]])
            rMat[ind1,ind2] = np.linalg.det(hMat)- k *np.square(np.trace(hMat))
    
    #Non maximum suppression
    supWindSize = 15
    supRMat = rMat.copy()
    paddedR = np.pad(rMat, int((supWindSize-1)/2), 'symmetric')
    for ind1 in range(image.shape[0]):
        for ind2 in range(image.shape[1]):
            supWind = paddedR[ind1:ind1+supWindSize, ind2:ind2+supWindSize]
            if not (np.max(supWind) == rMat[ind1, ind2]):
                supRMat[ind1, ind2] = 0 
    
    #Thresholding and returning a list of tuples
    pixel_coords = np.argwhere(supRMat > thresh)
    return pixel_coords
    