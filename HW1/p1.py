import cv2
import numpy as np

#Problem 1
def read_image(im_path):
    img = cv2.imread(im_path, 0)
    return img

#Problem 2
def histogram(gray_in):
    hist = np.zeros((256,))
    gray_vals = gray_in.flatten()
    for val in gray_vals:
        hist[val]+= 1
    return hist

#Problem 3
def denoisy_median_filtering(gray_in, diameter=3):
    padWid = int((diameter - 1)/2)
    padIm = np.pad(gray_in, padWid, 'edge')
    denoised_img = 0* gray_in.copy()
    for ind1 in range(gray_in.shape[0]):
        for ind2 in range(gray_in.shape[1]):
            seg = padIm[ind1:ind1 + diameter, ind2:ind2+diameter]
            denoised_img[ind1, ind2] = np.median(seg)
    return denoised_img

#Problem 4
def binarize(gray_in, threshold=128):
    binary_img = gray_in.copy()
    binary_img[binary_img > threshold] = 255
    binary_img[binary_img <= threshold] = 0
    return binary_img

#Problem 5
def sequential_label(binary_in):
    paddedIm = np.pad(binary_in, 1, 'constant', constant_values=(0))
    top_val = 255
    cur_label = 0
    equivalencies = list()
    labeledIm = paddedIm.copy().astype(int) * 0
    for ind1 in range(paddedIm.shape[0] -1):
        for ind2 in range(paddedIm.shape[1] -1):
            if(paddedIm[ind1 + 1, ind2 + 1] == top_val):
                if(np.sum(paddedIm[ind1:ind1+2, ind2:ind2+2]) == top_val):
                    cur_label += 1   
                    labeledIm[ind1 + 1, ind2 + 1] = cur_label
                elif(paddedIm[ind1, ind2] == top_val):
                    labeledIm[ind1+1, ind2+1] = labeledIm[ind1, ind2]
                elif(np.sum(paddedIm[ind1:ind1+2, ind2:ind2+2]) == 2*top_val):
                    if(paddedIm[ind1+1, ind2] == top_val):
                        labeledIm[ind1+1, ind2+1] = labeledIm[ind1+1, ind2]
                    if(paddedIm[ind1, ind2+1]== top_val):
                        labeledIm[ind1+1, ind2+1] = labeledIm[ind1, ind2+1]
                else:
                     if(labeledIm[ind1+1, ind2] == labeledIm[ind1, ind2+1]):
                        labeledIm[ind1+1, ind2+1] = labeledIm[ind1, ind2+1]
                     else:
                        newSet = True
                        labeledIm[ind1+1, ind2+1] = labeledIm[ind1+1, ind2]
                        for equiv in equivalencies:
                            if labeledIm[ind1, ind2+1] in equiv:
                                equiv.add(labeledIm[ind1+1, ind2])
                                newSet = False
                                break
                        if(newSet):
                            equivalencies.append(set([labeledIm[ind1, ind2+1], labeledIm[ind1+1, ind2]]))
    labeledIm = labeledIm[1:labeledIm.shape[0]-1, 1:labeledIm.shape[1]-1]
    
    newLabels = 1
    labeledImNew = labeledIm.copy()
    
    merged = True
    while(merged):
        merged = False
        for ind1 in range(len(equivalencies)):
            for ind2 in range(ind1 + 1, len(equivalencies)):
                if(len((equivalencies[ind1] & equivalencies[ind2])) > 0):
                    newSet = equivalencies[ind1]|equivalencies[ind2]
                    del equivalencies[ind2]
                    del equivalencies[ind1]
                    equivalencies.append(newSet)
                    merged = True
                    break
            if(merged):
                break
    
    
    for equiv in equivalencies:
        for val in equiv:
            labeledImNew[labeledIm == val] = newLabels
        newLabels += 1
    labelled_img = labeledImNew.astype(np.uint8)
    return labelled_img


#Problem 6
def compute_moment(labelled_in):
    labels = list(range(1, np.max(labelled_in)+1))
    moment_dict = dict()
    x_indices = labelled_in.copy().astype(int)
    y_indices = labelled_in.copy().astype(int)
    
    for x_ind in range(labelled_in.shape[0]):
        x_indices[x_ind,:] = x_ind
    for y_ind in range(labelled_in.shape[1]):
        y_indices[:, y_ind] = y_ind

    
    for lab in labels:
        moments = list()
        indicator = (labelled_in == lab).astype(int)
        
        m_00 = np.sum(indicator)
        moments.append(m_00)

        m_10 = np.sum(np.multiply(indicator, x_indices))
        moments.append(m_10)
        m_01 = np.sum(np.multiply(indicator, y_indices))
        moments.append(m_01)
    
        m_20 =  np.sum(np.multiply(indicator, np.square(x_indices)))
        m_11 =  np.sum(np.multiply(indicator, np.multiply(x_indices, y_indices)))
        m_02 =  np.sum(np.multiply(indicator, np.square(y_indices)))
        moments = moments + [m_20, m_11, m_02]
        
        x_ave = (1.0 * m_10)/m_00
        y_ave = (1.0 * m_01)/m_00
        centered_x = x_indices - x_ave
        centered_y = y_indices - y_ave
        
        mu_20 =  np.sum(np.multiply(indicator, np.square(centered_x)))
        mu_11 =  np.sum(np.multiply(indicator, np.multiply(centered_x, centered_y)))
        mu_02 =  np.sum(np.multiply(indicator, np.square(centered_y)))
        moments = moments + [mu_20, mu_11, mu_02]
        
        moment_dict[lab] = moments
    return moment_dict

#Problem 7
def compute_attribute(labelled_in):
    labels = list(range(1, np.max(labelled_in)+1))
    momentDict = compute_moment(labelled_in)
    attribute_dict = dict()
    
    for lab in labels:
        attributes = list()
        momentList = momentDict[lab]
        area = momentList[0]
        attributes.append(area)
        x_pos = momentList[1] * 1.0 / area
        y_pos = momentList[2] * 1.0 / area
        attributes.append((x_pos, y_pos))
        
        a = momentList[6]
        b = momentList[7]
        c = momentList[8]
        th1 = np.arctan2(b, a-c)/2
        th2 = np.arctan2(-b, c-a)/2
        E1 = a*((np.sin(th1))**2) - b * np.sin(th1)* np.cos(th1) + c * ((np.cos(th1))**2)
        E2 = a*((np.sin(th2))**2) - b * np.sin(th2)* np.cos(th1) + c * ((np.cos(th2))**2)
        
        if(E1 > E2):
            roundness = E2/E1
        else:
            roundness = E1/E2
        attributes.append(roundness)
        
        attribute_dict[lab] = attributes
    
    return attribute_dict
        
#Problem 8
def recognize_objects(new_img_path, attribute_dict):
    img = read_image(new_img_path)
    denoisedImg = denoisy_median_filtering(img)
    binaryIm = binarize(denoisedImg)
    labeledIm = sequential_label(binaryIm)
    attributeNew = compute_attribute(labeledIm)
    labels = list(range(1, np.max(labeledIm)+1))
    outputImg = labeledIm.copy()
    
    tol = 0.4
    round_tol = 0.2
    for lab in labels:
        matched = False
        for key in attribute_dict.keys():
            cond1 = np.absolute(np.array(attributeNew[lab][0]) - np.array(attribute_dict[key][0])) < tol * np.array(attribute_dict[key][0])
            cond4 = np.absolute(np.array(attributeNew[lab][2]) - np.array(attribute_dict[key][2])) < round_tol * np.array(attribute_dict[key][2])
            conditions = [cond1, cond4]
            if(np.sum(conditions) >= 2):
                matched = True
        if(matched):
            outputImg[labeledIm == lab] = 255
        else:
            outputImg[labeledIm == lab] = 0
    result_img = outputImg

    return result_img
