import cv2
import numpy as np
from scipy import signal
import math

def find_edge(gray_in, threshold):
    sobel1 = np.array([[-1, -2, -1],[0, 0, 0],[1, 2, 1]])
    sobel2 = np.transpose(sobel1)
    
    image = np.array(gray_in)
    grad1 = signal.convolve2d(image, sobel1, 'same')
    grad2 = signal.convolve2d(image, sobel2, 'same')
    grad_mag = np.sqrt(np.square(grad1) + np.square(grad2))
    grad_mag[grad_mag <= threshold] = 0
    grad_mag[grad_mag > threshold] = 255
    thresholded_edge_img = grad_mag
    return thresholded_edge_img 
   
def hough(edge_in, theta_nbin, rho_nbin):
    imCopy = edge_in.copy()
    imHeight, imWidth = edge_in.shape
    max_dist = np.sqrt(np.square(imWidth) + np.square(imHeight))
    max_dist = np.ceil(max_dist)
    deg_range = np.linspace(-90.0, 90.0, theta_nbin)
    theta_range = np.deg2rad(deg_range)
    rho_range = np.linspace(-max_dist, max_dist, rho_nbin)
    
    accumArr = np.zeros((rho_nbin, theta_nbin))
    cosTheta = np.cos(theta_range)
    sinTheta = np.sin(theta_range)
    
    x_ind, y_ind = np.where(imCopy > 0)
    
    for val in range(len(x_ind)):
        for theta_val in range(len(theta_range)):
            rho_val = round(x_ind[val] * sinTheta[theta_val] + y_ind[val] * cosTheta[theta_val])
            rho_val += max_dist
            rho_ind = int(rho_val * rho_nbin /(2.0 * max_dist))
            accumArr[rho_ind, theta_val] += 1
    
    accumArr = (255.0 * accumArr/ np.max(accumArr))
    hough_res = accumArr.astype(np.uint8)
    return hough_res



def hough_line(gray_in, accumulator_array, hough_threshold):
    gray_out = gray_in.copy().astype(np.uint8)
    imHeight, imWidth = gray_out.shape
    max_dist = np.sqrt(np.square(imWidth) + np.square(imHeight))
    rho_vals, theta_vals = np.where(accumulator_array > hough_threshold)
    theta_vals = (theta_vals * np.pi / accumulator_array.shape[1]) - np.pi/2
    rho_vals = (rho_vals * 2.0 *max_dist / accumulator_array.shape[0]) - max_dist
    for ind in range(len(rho_vals)):
        x1 = int(rho_vals[ind]*np.cos(theta_vals[ind]))
        y1 = int(rho_vals[ind]*np.sin(theta_vals[ind]))
        ortho = theta_vals[ind] + np.pi/2
        x2 = int(x1 + max_dist*np.cos(ortho))
        y2 = int(y1 + max_dist*np.sin(ortho))
        x3 = int(x1 - max_dist*np.cos(ortho))
        y3 = int(y1 - max_dist*np.sin(ortho))
        cv2.line(gray_out, (x2,y2), (x3,y3), (255, 255, 255), thickness=2)
    gray_out_with_edge = gray_out
    return gray_out
        